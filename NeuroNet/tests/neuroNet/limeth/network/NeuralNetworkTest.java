package neuroNet.limeth.network;

import neuroNet.limeth.network.functions.ActivationFunction;
import neuroNet.limeth.network.neurons.BiasNeuron;
import neuroNet.limeth.network.neurons.HiddenNeuron;
import neuroNet.limeth.network.neurons.InputNeuron;
import neuroNet.limeth.network.neurons.OutputNeuron;
import org.junit.Before;
import org.junit.Test;
import org.testng.Assert;

import static org.junit.Assert.*;

public class NeuralNetworkTest
{
    private float[][] expectedValueArray = new float[][] {
            {0, 0, 0},
            {1, 0, 0},
            {0, 1, 3},
            {1, 1, 3}
    };
    private NeuralNetwork network;
    private float inputA;
    private float inputB;
    private OutputNeuron outputNeuron;

    /*
    (A)  1 >  (HA)
       \     /    \
        -1
          X       1.0 > (O) >
         1
       /     \    /
    (B)  2 >  (HB)
     */

    @Before
    public void setUp() throws Exception
    {
        network = new NeuralNetwork(ActivationFunction.LINEAR, 1);

        //Input layer
        NeuralLayer inputLayer = new NeuralLayer(network).register();
        InputNeuron inputNeuronA = new InputNeuron(inputLayer, () -> (float) inputA).register();
        InputNeuron inputNeuronB = new InputNeuron(inputLayer, () -> (float) inputB).register();

        //Hidden layers
        NeuralLayer hiddenLayer = new NeuralLayer(network).register();
        HiddenNeuron hiddenNeuronA = new HiddenNeuron(hiddenLayer).register();
        HiddenNeuron hiddenNeuronB = new HiddenNeuron(hiddenLayer).register();

        inputNeuronA.getConnection(hiddenNeuronA).setWeight(1);
        inputNeuronB.getConnection(hiddenNeuronB).setWeight(2);
        inputNeuronA.getConnection(hiddenNeuronB).setWeight(-1);
        inputNeuronB.getConnection(hiddenNeuronA).setWeight(1);

        //Output layer
        NeuralLayer outputLayer = new NeuralLayer(network).register();
        outputNeuron = new OutputNeuron(outputLayer).register();

        hiddenNeuronA.getConnection(outputNeuron).setWeight(1);
        hiddenNeuronB.getConnection(outputNeuron).setWeight(1);
    }

    @Test
    public void testXOR()
    {
        for(float[] expectedValues : expectedValueArray)
        {
            network.resetOutputs();

            inputA = expectedValues[0];
            inputB = expectedValues[1];
            float expectedResult = expectedValues[2];
            float result = outputNeuron.getOutput();
/*
            if(result < 0)
                result = 0;
            else if(result > 1)
                result = 1;*/

            Assert.assertEquals(result, expectedResult, "[" + inputA + "; " + inputB + "] -> " +
                    result + " (expected: " + expectedResult + ")");
        }
    }
}