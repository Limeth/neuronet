package neuroNet.limeth.network;

import neuroNet.limeth.network.neurons.*;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.Set;
import java.util.function.Predicate;
import java.util.stream.Collectors;

@SuppressWarnings("serial")
public class NeuralLayer extends ArrayList<Neuron>
{
	private final NeuralNetwork network;
	
	public NeuralLayer(NeuralNetwork network)
	{
		super();
		
		if(network == null)
			throw new IllegalArgumentException("The network cannot be null!");
		
		this.network = network;
	}
	
	public NeuralLayer register(Integer index)
	{
		if(index != null)
			network.add(index, this);
		else
			network.add(this);
		
		return this;
	}
	
	public NeuralLayer register()
	{
		return register(null);
	}
	
	public static NeuralLayer createSimpleInputLayer(NeuralNetwork network, Integer index, int neurons, boolean biasNeuron)
	{
		NeuralLayer layer = new NeuralLayer(network).register(index);
		
		for(int i = 0; i < neurons; i++)
			new InputNeuron(layer).register();
		
		if(biasNeuron)
			new BiasNeuron(layer).register();
		
		return layer;
	}
	
	public static NeuralLayer createSimpleHiddenLayer(NeuralNetwork network, Integer index, int neurons, boolean biasNeuron)
	{
		NeuralLayer layer = new NeuralLayer(network).register(index);
		
		for(int i = 0; i < neurons; i++)
			new HiddenNeuron(layer).register();
		
		if(biasNeuron)
			new BiasNeuron(layer).register();
		
		return layer;
	}
	
	public static NeuralLayer createSimpleOutputLayer(NeuralNetwork network, Integer index, int neurons)
	{
		NeuralLayer layer = new NeuralLayer(network).register(index);
		
		for(int i = 0; i < neurons; i++)
			new OutputNeuron(layer).register();
		
		return layer;
	}
	
	public NeuralLayer getPreviousLayer()
	{
		int index = network.indexOf(this);
		
		if(index > 0)
			return network.get(index - 1);
		else
			return null;
	}
	
	public NeuralLayer getNextLayer()
	{
		int index = network.indexOf(this);
		
		if(index < network.size() - 1)
			return network.get(index + 1);
		else
			return null;
	}
	
	public boolean isFirst()
	{
		return network.indexOf(this) <= 0;
	}
	
	public boolean isLast()
	{
		return network.indexOf(this) >= network.size() - 1;
	}
	
	public Set<Neuron> get(Predicate<? super Neuron> predicate)
	{
		return stream().filter(predicate).collect(Collectors.toSet());
	}
	
	public NeuralConnectionSet getConnections()
	{
		NeuralConnectionSet set = new NeuralConnectionSet();
		
		for(Neuron neuron : this)
			set.addAll(neuron.getConnections());
		
		return set;
	}
	
	public NeuralConnectionSet getConnections(Predicate<? super NeuralConnection> predicate)
	{
		NeuralConnectionSet set = new NeuralConnectionSet();
		
		for(Neuron neuron : this)
			set.addAll(neuron.getConnections(predicate));
		
		return set;
	}
	
	@Override
	public boolean add(Neuron value) {
		return value != null && super.add(value);
	}
	
	@Override
	public void add(int index, Neuron value)
	{
		if(value == null)
			return;
		
		super.add(index, value);
	}
	
	@Override
	public boolean addAll(Collection<? extends Neuron> coll)
	{
		coll = coll.stream().filter(value -> value != null).map(Neuron.class::cast).collect(Collectors.toList());
		
		return super.addAll(coll);
	}
	
	@Override
	public boolean addAll(int index, Collection<? extends Neuron> coll)
	{
		coll = coll.stream().filter(value -> value != null).map(Neuron.class::cast).collect(Collectors.toList());
		
		return super.addAll(index, coll);
	}

	public NeuralNetwork getNetwork()
	{
		return network;
	}
	
	public int getIndex()
	{
		return network.indexOf(this);
	}
	
	@Override
	public String toString()
	{
		String result = getClass().getSimpleName() + " #" + getIndex() + " [";
		boolean first = true;
		
		for(Neuron neuron : this)
		{
			if(first)
				first = false;
			else
				result += "; ";
			
			result += neuron.toString(false);
		}
		
		return result + "]";
	}
	
	public NeuralLayer clone(NeuralNetwork network)
	{
		NeuralLayer result = new NeuralLayer(network).register();
		
		for(Neuron neuron : this)
			neuron.clone(result).register();
		
		return result;
	}

	public boolean equalsLayer(NeuralLayer o)
	{
		if(this == o)
			return true;
		if(o == null || getClass() != o.getClass())
			return false;

		if(size() != o.size())
			return false;

		Iterator<Neuron> thisIterator = iterator();
		Iterator<Neuron> thatIterator = o.iterator();

		while(thisIterator.hasNext())
			if(!thisIterator.next().equalsNeuron(thatIterator.next()))
				return false;

		return true;
	}
}
