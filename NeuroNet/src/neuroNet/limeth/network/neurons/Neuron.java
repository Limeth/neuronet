package neuroNet.limeth.network.neurons;

import com.google.gson.JsonObject;
import neuroNet.limeth.network.NeuralConnection;
import neuroNet.limeth.network.NeuralConnectionSet;
import neuroNet.limeth.network.NeuralLayer;
import neuroNet.limeth.network.NeuralNetwork;
import neuroNet.limeth.network.functions.ActivationFunction;

import java.util.function.Predicate;

/**
 * @author Limeth
 * 
 *  Every neuron class must contain the following constructor
 * 		{@code public Neuron(NeuralLayer, JsonObject)}
 */
public abstract class Neuron
{
	private final NeuralLayer layer;
	protected final NeuralConnectionSet connectionSet;
	private String name;
	private Float lazySum, lazyOutput;
	
	public Neuron(NeuralLayer layer, JsonObject jsonRoot)
	{
		if(layer == null)
			throw new IllegalArgumentException("The layer cannot be null!");
		
		this.layer = layer;
		connectionSet = new NeuralConnectionSet();
		
		if(jsonRoot != null)
		{
			if(jsonRoot.has("name"))
				name = jsonRoot.has("name") && jsonRoot.get("name").isJsonPrimitive() ? jsonRoot.get("name").getAsString() : null;
		}
	}
	
	public Neuron(NeuralLayer layer)
	{
		this(layer, null);
	}
	
	public JsonObject getAsJsonObject()
	{
		JsonObject root = new JsonObject();
		
		root.addProperty("name", name);
		
		return root;
	}
	
	/**
	 * Registers the neuron to the layer and network
	 */
	public abstract Neuron register();
	
	/**
	 * @param next The neuron the connection is going to
	 */
	public abstract boolean canProvideConnection(Neuron next);
	
	/**
	 * @param previous The neuron the connection is coming from
	 */
	public abstract boolean canAcceptConnection(Neuron previous);
	public abstract Neuron clone(NeuralLayer layer);
	
	public static boolean canConnect(Neuron first, Neuron second)
	{
		return first.canProvideConnection(second) && second.canAcceptConnection(first);
	}
	
	public void resetOutput()
	{
		lazySum = null;
		lazyOutput = null;
	}

	public float getSum()
	{
		return lazySum != null ? lazySum : calculateSum();
	}
	
	public float getOutput()
	{
		return lazyOutput != null ? lazyOutput : calculateOutput();
	}
	
	private float calculateSum()
	{
		NeuralConnectionSet inputSet = getConnections(NeuralConnectionSet.isOutput(this));
		
		float value = 0;
		
		for(NeuralConnection conn : inputSet)
		{
			float weight = conn.getWeight();
			Neuron other = conn.getOther(this);
			float otherValue = other.getOutput();
			
			value += otherValue * weight;
		}
		
		return lazySum = value;
	}
	
	private float calculateOutput()
	{
		NeuralNetwork network = getNetwork();
		ActivationFunction afc = network.getActivationFunction();
		float sum = getSum();
		
		return lazyOutput = afc.getValue(sum);
	}
	
	public boolean connect(Neuron neuron, float weight)
	{
		if(!canConnect(this, neuron))
			throw new IllegalArgumentException("Neurons " + this + " and " + neuron + " cannot be connected!");
		
		NeuralConnection conn = new NeuralConnection(this, neuron, weight);
		
		return connectionSet.add(conn) & neuron.connectionSet.add(conn);
	}
	
	public boolean connect(Neuron neuron)
	{
		NeuralNetwork network = getNetwork();
		float weightRange = network.getWeightRange();
		float weight = ((float) (Math.random()) * 2 - 1) * weightRange;
		
		return connect(neuron, weight);
	}
	
	public NeuralLayer getLayer()
	{
		return layer;
	}
	
	public NeuralNetwork getNetwork()
	{
		return layer.getNetwork();
	}
	
	public Neuron withName(String name)
	{
		setName(name);
		return this;
	}
	
	public void setName(String name)
	{
		this.name = name;
	}
	
	public String getName()
	{
		return name;
	}
	
	public int getIndex()
	{
		for(int i = 0; i < layer.size(); i++)
		{
			Neuron neuron = layer.get(i);

			if(neuron == this)
				return i;
		}

		throw new IllegalStateException("Neuron seems to be unregistered!");
	}
	
	public String toString(boolean showLayerIndex)
	{
		String label;
		
		if(name != null)
			label = name;
		else
		{
			Class<? extends Neuron> clazz = getClass();
			
			label = clazz.isAnonymousClass() ? clazz.getTypeName() : clazz.getSimpleName();
		}
		
		NeuralNetwork network = getNetwork();
		int index = network.indexOf(layer);
		
		if(showLayerIndex)
			label += " l" + index;
		
		return label;
	}
	
	@Override
	public String toString()
	{
		return toString(true);
	}
	
	public NeuralConnection getConnection(Neuron other)
	{
		for(NeuralConnection connection : connectionSet)
			if(connection.contains(this, other))
				return connection;
		
		return null;
	}
	
	public NeuralConnectionSet getConnections()
	{
		return (NeuralConnectionSet) connectionSet.clone();
	}
	
	public NeuralConnectionSet getProvidedConnections()
	{
		return getConnections(isProvided);
	}
	
	public NeuralConnectionSet getAcceptedConnections()
	{
		return getConnections(isAccepted);
	}
	
	public NeuralConnectionSet getConnections(Predicate<? super NeuralConnection> predicate)
	{
		return getConnections().getAll(predicate);
	}

	public final Predicate<? super NeuralConnection> isProvided = connection -> connection.getFrom() == this;

	public final Predicate<? super NeuralConnection> isAccepted = connection -> connection.getTo() == this;

	public boolean equalsNeuron(Neuron neuron)
	{
		if(this == neuron)
			return true;
		if(neuron == null || getClass() != neuron.getClass())
			return false;
		if(name != null ? !name.equals(neuron.name) : neuron.name != null)
			return false;

		return true;
	}
}
