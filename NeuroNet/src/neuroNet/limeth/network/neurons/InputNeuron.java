package neuroNet.limeth.network.neurons;

import com.google.gson.JsonObject;
import neuroNet.limeth.network.NeuralLayer;
import neuroNet.limeth.network.NeuralNetwork;
import neuroNet.limeth.network.adapters.OutputResetListener;
import neuroNet.limeth.network.functions.ActivationFunction;

import java.util.function.Supplier;

public class InputNeuron extends Neuron
{
	private Supplier<Float> inputSource;
	private OutputResetListener outputResetListener;
	
	public InputNeuron(NeuralLayer layer, JsonObject jsonRoot)
	{
		super(layer, jsonRoot);
	}
	
	public InputNeuron(NeuralLayer layer, Supplier<Float> inputSource, OutputResetListener outputResetListener)
	{
		super(layer);
		
		this.inputSource = inputSource;
		this.outputResetListener = outputResetListener;
	}
	
	public InputNeuron(NeuralLayer layer, Supplier<Float> inputSource)
	{
		this(layer, inputSource, null);
	}
	
	public InputNeuron(NeuralLayer layer)
	{
		super(layer);
	}
	
	@Override
	public InputNeuron register()
	{
		NeuralLayer layer = getLayer();
		
		if(!layer.isLast())
		{
			NeuralLayer nextLayer = layer.getNextLayer();
			
			for(Neuron neuron : nextLayer)
				if(Neuron.canConnect(this, neuron))
					connect(neuron);
		}

		layer.add(this);
		
		return this;
	}
	
	@Override
	public void resetOutput()
	{
		super.resetOutput();
		
		if(outputResetListener != null)
			outputResetListener.onOuputReset();
	}
	
	@Override
	public float getOutput()
	{
		if(inputSource == null)
			throw new RuntimeException("Cannot get output, the neural input supplier is not specified!");
		
		NeuralNetwork network = getNetwork();
		ActivationFunction afc = network.getActivationFunction();
		float input = inputSource.get();
		
		return afc.getValue(input);
	}
	
	@Override
	public boolean canProvideConnection(Neuron neuron)
	{
		NeuralNetwork network = getNetwork();
		NeuralLayer my = getLayer();
		NeuralLayer its = neuron.getLayer();
		int myIndex = network.indexOf(my);
		int itsIndex = network.indexOf(its);
		
		return myIndex < itsIndex;
	}
	
	@Override
	public boolean canAcceptConnection(Neuron previous)
	{
		return false;
	}
	
	@Override
	public InputNeuron withName(String name)
	{
		return (InputNeuron) super.withName(name);
	}
	
	public OutputResetListener getOutputResetListener()
	{
		return outputResetListener;
	}
	
	public void setOutputResetListener(OutputResetListener outputResetListener)
	{
		this.outputResetListener = outputResetListener;
	}
	
	public Supplier<Float> getInputSource()
	{
		return inputSource;
	}
	
	public void setSupplier(Supplier<Float> inputSource)
	{
		this.inputSource = inputSource;
	}

	public void removeSupplier()
	{
		setSupplier(null);
	}
	
	@Override
	public Neuron clone(NeuralLayer layer)
	{
		return new InputNeuron(layer, inputSource, outputResetListener).withName(getName());
	}
}
