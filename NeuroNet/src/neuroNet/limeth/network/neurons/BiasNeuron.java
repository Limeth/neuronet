package neuroNet.limeth.network.neurons;

import neuroNet.limeth.network.NeuralLayer;
import neuroNet.limeth.network.NeuralNetwork;

import com.google.gson.JsonObject;


public class BiasNeuron extends Neuron
{
	public float value;
	
	public BiasNeuron(NeuralLayer layer, JsonObject jsonRoot)
	{
		super(layer, jsonRoot);
		
		if(jsonRoot != null)
		{
			if(jsonRoot.has("value"))
				value = jsonRoot.get("value").getAsFloat();
		}
	}
	
	public BiasNeuron(NeuralLayer layer, float value)
	{
		super(layer);
		
		this.value = value;
	}
	
	public BiasNeuron(NeuralLayer layer)
	{
		this(layer, 1);
	}
	
	@Override
	public JsonObject getAsJsonObject()
	{
		JsonObject root = super.getAsJsonObject();
		
		root.addProperty("value", value);
		
		return root;
	}
	
	@Override
	public BiasNeuron register()
	{
		NeuralLayer layer = getLayer();
		
		if(!layer.isLast())
		{
			NeuralLayer nextLayer = layer.getNextLayer();
			
			for(Neuron neuron : nextLayer)
				if(Neuron.canConnect(this, neuron))
					connect(neuron);
		}
		
		layer.add(this);
		
		return this;
	}
	
	@Override
	public float getOutput()
	{
		return value;
	}
	
	public void setValue(float value)
	{
		this.value = value;
	}
	
	@Override
	public boolean canProvideConnection(Neuron neuron)
	{
		NeuralNetwork network = getNetwork();
		NeuralLayer my = getLayer();
		NeuralLayer its = neuron.getLayer();
		int myIndex = network.indexOf(my);
		int itsIndex = network.indexOf(its);
		
		return itsIndex > myIndex;
	}
	
	@Override
	public boolean canAcceptConnection(Neuron previous)
	{
		return false;
	}
	
	@Override
	public Neuron clone(NeuralLayer layer)
	{
		return new BiasNeuron(layer, value).withName(getName());
	}

	@Override
	public boolean equalsNeuron(Neuron neuron)
	{
		if(!super.equalsNeuron(neuron))
			return false;

		return value == ((BiasNeuron) neuron).value;
	}
}
