package neuroNet.limeth.network.neurons;

import neuroNet.limeth.network.NeuralLayer;
import neuroNet.limeth.network.NeuralNetwork;

import com.google.gson.JsonObject;

public class OutputNeuron extends Neuron
{
	public OutputNeuron(NeuralLayer layer, JsonObject jsonRoot)
	{
		super(layer, jsonRoot);
	}
	
	public OutputNeuron(NeuralLayer layer)
	{
		super(layer);
	}
	
	@Override
	public OutputNeuron register()
	{
		NeuralLayer layer = getLayer();
		
		if(!layer.isFirst())
		{
			NeuralLayer previousLayer = layer.getPreviousLayer();
			
			for(Neuron neuron : previousLayer)
				if(Neuron.canConnect(neuron, this))
					neuron.connect(this);
		}
		
		layer.add(this);
		
		return this;
	}
	
	@Override
	public OutputNeuron withName(String name)
	{
		return (OutputNeuron) super.withName(name);
	}
	
	@Override
	public boolean canProvideConnection(Neuron neuron)
	{
		return false;
	}
	
	@Override
	public boolean canAcceptConnection(Neuron neuron)
	{
		NeuralNetwork network = getNetwork();
		NeuralLayer my = getLayer();
		NeuralLayer its = neuron.getLayer();
		int myIndex = network.indexOf(my);
		int itsIndex = network.indexOf(its);
		
		return itsIndex < myIndex;
	}
	
	@Override
	public Neuron clone(NeuralLayer layer)
	{
		return new OutputNeuron(layer).withName(getName());
	}
}
