package neuroNet.limeth.network.neurons;

import neuroNet.limeth.network.NeuralLayer;
import neuroNet.limeth.network.NeuralNetwork;

import com.google.gson.JsonObject;

public class HiddenNeuron extends Neuron
{
	public HiddenNeuron(NeuralLayer layer, JsonObject jsonRoot)
	{
		super(layer, jsonRoot);
	}
	
	public HiddenNeuron(NeuralLayer layer)
	{
		super(layer);
	}
	
	@Override
	public HiddenNeuron register()
	{
		NeuralLayer layer = getLayer();
		
		if(!layer.isFirst())
		{
			NeuralLayer previousLayer = layer.getPreviousLayer();
			
			for(Neuron neuron : previousLayer)
				if(Neuron.canConnect(neuron, this))
					neuron.connect(this);
		}
		
		if(!layer.isLast())
		{
			NeuralLayer nextLayer = layer.getNextLayer();
			
			for(Neuron neuron : nextLayer)
				if(Neuron.canConnect(this, neuron))
					connect(neuron);
		}

		layer.add(this);
		
		return this;
	}
	
	@Override
	public boolean canProvideConnection(Neuron neuron)
	{
		NeuralNetwork network = getNetwork();
		NeuralLayer my = getLayer();
		NeuralLayer its = neuron.getLayer();
		int myIndex = network.indexOf(my);
		int itsIndex = network.indexOf(its);
		
		return myIndex < itsIndex;
	}
	
	@Override
	public boolean canAcceptConnection(Neuron neuron)
	{
		NeuralNetwork network = getNetwork();
		NeuralLayer my = getLayer();
		NeuralLayer its = neuron.getLayer();
		int myIndex = network.indexOf(my);
		int itsIndex = network.indexOf(its);
		
		return myIndex > itsIndex;
	}
	
	@Override
	public Neuron clone(NeuralLayer layer)
	{
		return new HiddenNeuron(layer).withName(getName());
	}
}
