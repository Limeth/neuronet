package neuroNet.limeth.network.adapters;

import neuroNet.limeth.network.neurons.InputNeuron;

public abstract class InputNeuronAdapter
{
	public abstract void setSuppliers(InputNeuron... neurons);
}
