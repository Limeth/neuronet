package neuroNet.limeth.network.adapters;

import java.util.Objects;

import neuroNet.limeth.network.neurons.OutputNeuron;

public abstract class OutputNeuronAdapter<T>
{
	private OutputNeuron[] neurons;
	
	public OutputNeuronAdapter(OutputNeuron... neurons)
	{
		Objects.requireNonNull(neurons, "The neurons must not be null!");
		
		this.neurons = neurons;
	}
	
	public abstract T getValue();

	public OutputNeuron getNeuron(int index)
	{
		return neurons[index];
	}
	
	public OutputNeuron[] getNeurons()
	{
		return neurons;
	}
	
	public void setNeurons(OutputNeuron... neurons)
	{
		Objects.requireNonNull(neurons, "The neurons must not be null!");
		
		this.neurons = neurons;
	}
}
