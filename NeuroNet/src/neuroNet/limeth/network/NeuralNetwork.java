package neuroNet.limeth.network;

import com.google.gson.annotations.Expose;
import neuroNet.limeth.network.functions.ActivationFunction;
import neuroNet.limeth.network.neurons.Neuron;

import java.util.*;
import java.util.function.Predicate;
import java.util.stream.Collectors;

@SuppressWarnings("serial")
public class NeuralNetwork extends ArrayList<NeuralLayer>
{
	@Expose private ActivationFunction activationFunction;
	@Expose private float weightRange;
	
	public NeuralNetwork(ActivationFunction activationFunction, float weightRange)
	{
		super();
		
		if(activationFunction == null)
			throw new IllegalArgumentException("The activation function cannot be null!");
		else if(weightRange < 0)
			throw new IllegalArgumentException("The weight range must be positive!");
		
		this.weightRange = weightRange;
		this.activationFunction = activationFunction;
	}
	
	/**
	 * Constructs a new NeuralNetwork with weight range: 1
	 */
	public NeuralNetwork(ActivationFunction activationFunction)
	{
		this(activationFunction, 1);
	}
	
	/**
	 * Constructs a new NeuralNetwork with activation function SIGMOID and weight range: 1
	 */
	public NeuralNetwork()
	{
		this(ActivationFunction.SIGMOID);
	}
	
	public Set<NeuralLayer> getLayers(Predicate<? super NeuralLayer> predicate)
	{
		return stream().filter(predicate).collect(Collectors.toSet());
	}
	
	public ArrayList<Neuron> getNeurons(Predicate<? super Neuron> predicate)
	{
		ArrayList<Neuron> set = new ArrayList<>();
		
		for(NeuralLayer layer : this)
			set.addAll(layer.get(predicate));
		
		return set;
	}
	
	public List<Neuron> getNeurons()
	{
		List<Neuron> set = new ArrayList<>();

		this.forEach(set::addAll);

		return set;
	}
	
	public NeuralConnectionSet getConnections()
	{
		NeuralConnectionSet set = new NeuralConnectionSet();
		List<Neuron> neurons = getNeurons();
		
		for(Neuron neuron : neurons)
			set.addAll(neuron.getConnections());
		
		return set;
	}
	
	public NeuralConnectionSet getConnections(Predicate<? super NeuralConnection> predicate)
	{
		NeuralConnectionSet set = new NeuralConnectionSet();
		List<Neuron> neurons = getNeurons();
		
		for(Neuron neuron : neurons)
			set.addAll(neuron.getConnections(predicate));
		
		return set;
	}
	
	public void resetOutputs()
	{
		getNeurons().forEach(Neuron::resetOutput);
	}

	@Override
	public void add(int index, NeuralLayer value)
	{
		if(!canBeAdded(value))
			return;
		
		super.add(index, value);
	}
	
	@Override
	public boolean add(NeuralLayer value) {
		return canBeAdded(value) && super.add(value);
	}
	
	public NeuralLayer getFirst()
	{
		return get(0);
	}
	
	public NeuralLayer getLast()
	{
		return get(size() - 1);
	}
	
	@Override
	public boolean addAll(Collection<? extends NeuralLayer> coll)
	{
		coll = coll.stream().filter(this::canBeAdded).map(NeuralLayer.class::cast).collect(Collectors.toList());
		
		return super.addAll(coll);
	}
	
	@Override
	public boolean addAll(int index, Collection<? extends NeuralLayer> coll)
	{
		coll = coll.stream().filter(this::canBeAdded).map(NeuralLayer.class::cast).collect(Collectors.toList());
		
		return super.addAll(index, coll);
	}
	
	private boolean canBeAdded(NeuralLayer value)
	{
		return value != null && !containsStrict(value);
	}
	
	public boolean containsStrict(NeuralLayer layer)
	{
		for(NeuralLayer current : this)
			if(current == layer)
				return true;
		
		return false;
	}

	public ActivationFunction getActivationFunction()
	{
		return activationFunction;
	}

	public void setActivationFunction(ActivationFunction activationFunction)
	{
		if(activationFunction == null)
			throw new IllegalArgumentException("The activation function cannot be null!");
		
		this.activationFunction = activationFunction;
	}

	public float getWeightRange()
	{
		return weightRange;
	}

	public void setWeightRange(float weightRange)
	{
		if(weightRange < 0)
			throw new IllegalArgumentException("The weight range must be positive!");
		
		this.weightRange = weightRange;
	}
	
	@Override
	public String toString()
	{
		String result = getClass().getSimpleName() + " [\n\tActivation function: "
				+ activationFunction + "\n\tWeight range: " + weightRange + '\n';
		boolean first = true;
		
		for(NeuralLayer layer : this)
		{
			if(first)
				first = false;
			else
				result += ";\n";
			
			result += '\t' + layer.toString();
		}
		
		return result + "\n]";
	}
	
	@Override
	public NeuralNetwork clone()
	{
		NeuralNetwork result = new NeuralNetwork(activationFunction, weightRange);
		
		//Clone layers
		for(NeuralLayer layer : this)
			layer.clone(result);
		
		//Rewrite connections
		for(int layerIndex = 0; layerIndex < size(); layerIndex++)
		{
			NeuralLayer sourceLayer = get(layerIndex);
			NeuralLayer targetLayer = result.get(layerIndex);
			
			for(int neuronIndex = 0; neuronIndex < sourceLayer.size(); neuronIndex++)
			{
				Neuron sourceNeuron = sourceLayer.get(neuronIndex);
				Neuron targetNeuron = targetLayer.get(neuronIndex);
				NeuralConnectionSet sourceProvidedConnections = sourceNeuron.getProvidedConnections();
				
				for(NeuralConnection sourceProvidedConnection : sourceProvidedConnections)
				{
					Neuron sourceNeuronAcceptor = sourceProvidedConnection.getTo();
					NeuralLayer sourceAcceptorLayer = sourceNeuronAcceptor.getLayer();
					int acceptorLayerIndex = sourceAcceptorLayer.getIndex();
					int acceptorNeuronIndex = sourceNeuronAcceptor.getIndex();
					NeuralLayer targetAcceptorLayer = result.get(acceptorLayerIndex);
					Neuron targetAcceptorNeuron = targetAcceptorLayer.get(acceptorNeuronIndex);
					NeuralConnection targetOfferedConnection = targetNeuron.getConnection(targetAcceptorNeuron);
					float weight = sourceProvidedConnection.getWeight();
					
					targetOfferedConnection.setWeight(weight);
				}
			}
		}
		
		return result;
	}

	@Override
	public boolean equals(Object o)
	{
		if(this == o)
			return true;
		if(o == null || getClass() != o.getClass())
			return false;
		/*if(!super.equals(o))
			return false;*/

		NeuralNetwork that = (NeuralNetwork) o;

		if(size() != that.size())
			return false;

		Iterator<NeuralLayer> thisIterator = iterator();
		Iterator<NeuralLayer> thatIterator = that.iterator();

		while(thisIterator.hasNext())
			if(!thisIterator.next().equalsLayer(thatIterator.next()))
				return false;

		if(Float.compare(that.weightRange, weightRange) != 0)
			return false;
		if(!activationFunction.equals(that.activationFunction))
			return false;
		if(!getConnections().equalsSet(((NeuralNetwork) o).getConnections()))
			return false;

		return true;
	}

	@Override
	public int hashCode()
	{
		int result = super.hashCode();
		result = 31 * result + activationFunction.hashCode();
		result = 31 * result + (weightRange != +0.0f ? Float.floatToIntBits(weightRange) : 0);
		return result;
	}
}
