package neuroNet.limeth.network.functions;

import java.util.HashMap;
import java.util.Map;

public enum ActivationFunction
{
	LINEAR()
	{
		@Override
		public float getValue(float value)
		{
			return value;
		}

		@Override
		public float derive(float value)
		{
			throw new UnsupportedOperationException();//TODO
		}
	},
	SIGMOID()
	{
		@Override
		public float getValue(float value)
		{
			return (float) (1.0 / (1.0 + Math.exp(-1 * value)));
		}

		@Override
		public float derive(float value)
		{
			return getValue(value) * (1 - getValue(value));
		}
	},
	TANH()
	{
		@Override
		public float getValue(float value)
		{
			return (float) Math.tanh(value);
		}

		@Override
		public float derive(float value)
		{
			throw new UnsupportedOperationException();//TODO
		}
	};

	public abstract float getValue(float value);
	public abstract float derive(float value);
}
