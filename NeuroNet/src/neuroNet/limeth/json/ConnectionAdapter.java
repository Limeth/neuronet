package neuroNet.limeth.json;

import java.lang.reflect.Type;

import neuroNet.limeth.network.NeuralConnection;
import neuroNet.limeth.network.NeuralLayer;
import neuroNet.limeth.network.NeuralNetwork;
import neuroNet.limeth.network.neurons.Neuron;

import com.google.gson.JsonDeserializationContext;
import com.google.gson.JsonDeserializer;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParseException;
import com.google.gson.JsonSerializationContext;
import com.google.gson.JsonSerializer;

public class ConnectionAdapter implements JsonSerializer<NeuralConnection>, JsonDeserializer<NeuralConnection>
{
	private final NeuralNetwork network;
	
	public ConnectionAdapter(NeuralNetwork network)
	{
		this.network = network;
	}
	
	@Override
	public JsonElement serialize(NeuralConnection src, Type typeOfSrc, JsonSerializationContext context)
	{
		JsonObject jsonConnection = new JsonObject();
		JsonObject jsonFrom = new JsonObject();
		JsonObject jsonTo = new JsonObject();
		Neuron fromNeuron = src.getFrom();
		Neuron toNeuron = src.getTo();
		NeuralLayer fromLayer = fromNeuron.getLayer();
		NeuralLayer toLayer = toNeuron.getLayer();
		int fromNeuronIndex = fromNeuron.getIndex();
		int toNeuronIndex = toNeuron.getIndex();
		int fromLayerIndex = fromLayer.getIndex();
		int toLayerIndex = toLayer.getIndex();
		float weight = src.getWeight();
		
		jsonFrom.addProperty("layer", fromLayerIndex);
		jsonFrom.addProperty("neuron", fromNeuronIndex);
		jsonTo.addProperty("layer", toLayerIndex);
		jsonTo.addProperty("neuron", toNeuronIndex);
		jsonConnection.add("from", jsonFrom);
		jsonConnection.add("to", jsonTo);
		jsonConnection.addProperty("weight", weight);
		
		return jsonConnection;
	}
	
	@Override
	public NeuralConnection deserialize(JsonElement json, Type typeOfT, JsonDeserializationContext context) throws JsonParseException
	{
		if(network == null)
			throw new JsonParseException("The network was not specified, cannot deserialize.");
		
		JsonObject root = json.getAsJsonObject();
		JsonObject fromJson = root.get("from").getAsJsonObject();
		JsonObject toJson = root.get("to").getAsJsonObject();
		int fromLayerIndex = fromJson.get("layer").getAsInt();
		int fromNeuronIndex = fromJson.get("neuron").getAsInt();
		int toLayerIndex = toJson.get("layer").getAsInt();
		int toNeuronIndex = toJson.get("neuron").getAsInt();
		float weight = root.get("weight").getAsFloat();
		NeuralLayer fromLayer = network.get(fromLayerIndex);
		NeuralLayer toLayer = network.get(toLayerIndex);
		Neuron fromNeuron = fromLayer.get(fromNeuronIndex);
		Neuron toNeuron = toLayer.get(toNeuronIndex);
		NeuralConnection connection = fromNeuron.getConnection(toNeuron);
		
		connection.setWeight(weight);
		
		return connection;
	}
	
	public NeuralNetwork getNetwork()
	{
		return network;
	}
}
