package neuroNet.limeth.json;

import com.google.gson.*;
import neuroNet.limeth.network.NeuralConnection;
import neuroNet.limeth.network.NeuralLayer;
import neuroNet.limeth.network.NeuralNetwork;
import neuroNet.limeth.network.functions.ActivationFunction;

import java.lang.reflect.Type;

public class NetworkAdapter implements JsonSerializer<NeuralNetwork>, JsonDeserializer<NeuralNetwork>
{
	private static final NetworkAdapter INSTANCE = new NetworkAdapter();
	
	private NetworkAdapter() {}
	
	@Override
	public JsonElement serialize(NeuralNetwork src, Type typeOfSrc, JsonSerializationContext context)
	{
		JsonObject root = new JsonObject();
		JsonArray layers = new JsonArray();
		JsonArray connections = new JsonArray();
		
		for(NeuralLayer layer : src)
			layers.add(context.serialize(layer));
		
		for(NeuralConnection connection : src.getConnections())
			connections.add(context.serialize(connection));

		root.add("activationFunction", context.serialize(src.getActivationFunction()));
		root.addProperty("weightRange", src.getWeightRange());
		root.add("layers", layers);
		root.add("connections", connections);
		
		return root;
	}
	
	@Override
	public NeuralNetwork deserialize(JsonElement json, Type typeOfT, JsonDeserializationContext context) throws JsonParseException
	{
		JsonObject root = json.getAsJsonObject();
		NeuralNetwork network = new NeuralNetwork();
		
		if(root.has("activationFunction") && root.get("activationFunction").isJsonPrimitive())
			network.setActivationFunction(context.deserialize(root.get("activationFunction"), ActivationFunction.class));
		
		if(root.has("weightRange") && root.get("weightRange").isJsonPrimitive())
			network.setWeightRange(root.get("weightRange").getAsFloat());
		
		if(root.has("layers") && root.get("layers").isJsonArray())
		{
			LayerAdapter layerAdapter = new LayerAdapter(network);
			
			for(JsonElement element : root.get("layers").getAsJsonArray())
				layerAdapter.deserialize(element, NeuralLayer.class, context);
		}
		
		if(root.has("connections") && root.get("connections").isJsonArray())
		{
			ConnectionAdapter adapter = new ConnectionAdapter(network);
			
			for(JsonElement element : root.get("connections").getAsJsonArray())
				adapter.deserialize(element, NeuralConnection.class, context);
		}
		
		return network;
	}
	
	public static NetworkAdapter getInstance()
	{
		return INSTANCE;
	}
}
