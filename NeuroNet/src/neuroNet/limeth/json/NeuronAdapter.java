package neuroNet.limeth.json;

import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Type;

import neuroNet.limeth.network.NeuralLayer;
import neuroNet.limeth.network.neurons.Neuron;

import com.google.gson.JsonDeserializationContext;
import com.google.gson.JsonDeserializer;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParseException;
import com.google.gson.JsonSerializationContext;
import com.google.gson.JsonSerializer;

public class NeuronAdapter implements JsonSerializer<Neuron>, JsonDeserializer<Neuron>
{
	private final NeuralLayer layer;
	
	public NeuronAdapter(NeuralLayer layer)
	{
		this.layer = layer;
	}
	
	@Override
	public JsonElement serialize(Neuron src, Type typeOfSrc, JsonSerializationContext context)
	{
		JsonObject root = src.getAsJsonObject();
		
		root.addProperty("type", src.getClass().getCanonicalName());
		
		return root;
	}
	
	@Override
	public Neuron deserialize(JsonElement json, Type typeOfT, JsonDeserializationContext context) throws JsonParseException
	{
		if(layer == null)
			throw new JsonParseException("The layer was not specified, cannot deserialize.");
		
		JsonObject root = json.getAsJsonObject();
		String type = root.get("type").getAsString();
		Class<?> clazz;
		Constructor<?> constructor;
		Neuron neuron;
		
		try
		{
			clazz = Class.forName(type);
			constructor = clazz.getConstructor(NeuralLayer.class, JsonObject.class);
			neuron = (Neuron) constructor.newInstance(layer, json);
		}
		catch(ClassNotFoundException | NoSuchMethodException | SecurityException | InstantiationException
				| IllegalAccessException | IllegalArgumentException | InvocationTargetException | ClassCastException e)
		{
			throw new JsonParseException(e);
		}
		
		return neuron;
	}
	
	public NeuralLayer getLayer()
	{
		return layer;
	}
}
