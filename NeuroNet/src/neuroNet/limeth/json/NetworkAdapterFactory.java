package neuroNet.limeth.json;

import com.google.gson.GsonBuilder;
import neuroNet.limeth.network.NeuralConnection;
import neuroNet.limeth.network.NeuralLayer;
import neuroNet.limeth.network.NeuralNetwork;
import neuroNet.limeth.network.neurons.Neuron;

public class NetworkAdapterFactory
{
	private NetworkAdapterFactory() {}
	
	public static GsonBuilder applyGsonSettings(GsonBuilder builder)
	{
		return builder.registerTypeAdapter(NeuralNetwork.class, NetworkAdapter.getInstance())
				.registerTypeAdapter(NeuralLayer.class, new LayerAdapter(null))
				.registerTypeAdapter(Neuron.class, new NeuronAdapter(null))
				.registerTypeAdapter(NeuralConnection.class, new ConnectionAdapter(null));
	}
	
	public static GsonBuilder createGsonBuilder()
	{
		return applyGsonSettings(new GsonBuilder());
	}
}
