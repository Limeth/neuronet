package neuroNet.limeth.json;

import java.lang.reflect.Type;

import neuroNet.limeth.network.NeuralLayer;
import neuroNet.limeth.network.NeuralNetwork;
import neuroNet.limeth.network.neurons.Neuron;

import com.google.gson.JsonArray;
import com.google.gson.JsonDeserializationContext;
import com.google.gson.JsonDeserializer;
import com.google.gson.JsonElement;
import com.google.gson.JsonParseException;
import com.google.gson.JsonSerializationContext;
import com.google.gson.JsonSerializer;

public class LayerAdapter implements JsonSerializer<NeuralLayer>, JsonDeserializer<NeuralLayer>
{
	private final NeuralNetwork network;
	
	public LayerAdapter(NeuralNetwork network)
	{
		this.network = network;
	}
	
	@Override
	public JsonElement serialize(NeuralLayer src, Type typeOfSrc, JsonSerializationContext context)
	{
		JsonArray root = new JsonArray();
		
		for(Neuron neuron : src)
			root.add(context.serialize(neuron, Neuron.class));
		
		return root;
	}
	
	@Override
	public NeuralLayer deserialize(JsonElement json, Type typeOfT, JsonDeserializationContext context) throws JsonParseException
	{
		if(network == null)
			throw new JsonParseException("The network was not specified, cannot deserialize.");
		
		JsonArray array = json.getAsJsonArray();
		NeuralLayer layer = new NeuralLayer(network).register();
		NeuronAdapter neuronAdapter = new NeuronAdapter(layer);
		
		for(JsonElement element : array)
			neuronAdapter.deserialize(element, Neuron.class, context).register();
		
		return layer;
	}
	
	public NeuralNetwork getNetwork()
	{
		return network;
	}
}
